# Electronic System Level Design of Heterogeneous Systems: a Motor Speed Control System Case Study

------------------------------------------------------------------------------

    README
    SystemC CT: SystemC Continuous Time Library

    Breytner FERNANDEZ <Breytner.Fernandez@univ-grenoble-alpes.fr>
    Liliana ANDRADE <Liliana.Andrade@univ-grenoble-alpes.fr>
    Frédéric Pétrot <Frederic.Petrot@univ-grenoble-alpes.fr>

------------------------------------------------------------------------------

Copyright (C) 2018-2021
Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
*Institute of Engineering Univ. Grenoble Alpes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

------------------------------------------------------------------------------


## Description: 

This repository contains the implementation of a case study on system-level modeling of a PID algorithm for the control of a DC motor speed. 
This case study is completely described in the article *[Electronic System Level Design of Heterogeneous Systems: a Motor Speed Control System Case Study](https://www.doi.org/10.1109/NEWCAS44328.2019.8961289)* [1] published in the proceedings of the 17th IEEE International New Circuits and Systems Conference (NEWCAS 2019).

We provide different equivalent models: 

1. A TLM model (virtual protoype) with a RISC-V Instruction Set Simulator that runs the digital PID control algorithm written in C, connected to a DC Motor model via digital-to-analog and analog-to-digital converters (tlm_iss_motor/)

![tlm](./doc/tlm.png)

2. An equivalent TLM model with a RISC-V QEMU Emulator encapsulated by [RABBITS](https://www.greensocs.com/fr_FR/solutions-1-1) for better simulation speed (tlm_qemu_rabbits_motor/)


3. An equivalent digital executable specification (motor_speed/de_controller/)

![tde](./doc/de.png)


4. A purely analog PID controller and DC motor modeled as transfer functions in the Timed Data Flow Model of Computation of SystemC AMS (tdf_controller/).

![tdf](./doc/tdf.png)


5. An equivalent purely analog PID controller and DC motor modeled as block diagrams in the Linear Signal Flow Model of Computation of SystemC AMS (lsf_controller/).

![tlm](./doc/lsf.png)


6.  An equivalent purely analog PID controller and DC motor modeled as electrical circuits in the Electrical Linear Networks Model of Computation of SystemC AMS (eln_controller/).

![eln](./doc/eln.png)


The following image shows the controlled motor speed for the different models:

![results](./doc/results.png)


Inside each folder you will find dependencies information and execution instructions.

### Authors:
* Breytner Fernández-Mesa
* Liliana Andrade
* Frédéric Pétrot

### Affiliation:
Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France

\* Institute of Engineering Univ. Grenoble Alpes

For missing features or bugs, please send an e-mail to:
    Breytner.Fernandez@univ-grenoble-alpes.fr
    Liliana.Andrade@univ-grenoble-alpes.fr
    Frederic.Petrot@univ-grenoble-alpes.fr

------------------------------------------------------------------------------
